import QtQuick 2.2
import Ubuntu.Components 1.3
import QtPositioning 5.2
import QtSensors 5.2
import QtQuick.Window 2.2

/*!
    \brief MainView with a Label and Button elements.
*/

Window {
    id: sensorsStatus_app
    title: i18n.tr('SensorsStatus')
    width: units.gu(150)
    height: units.gu(100)
    minimumWidth: units.gu(45)
    minimumHeight: units.gu(45)
    maximumWidth: Screen.width
    maximumHeight: Screen.height

    property real gnalMargins: units.gu(4)
    property real colSpacing: units.gu(2)

    function printablePositionMethod(method) {
        var out = i18n.tr('Unknown position method');
        if (method === PositionSource.SatellitePositioningMethod) out = i18n.tr('Satellite');
        else if (method === PositionSource.NoPositioningMethod) out = i18n.tr('Not available');
        else if (method === PositionSource.NonSatellitePositioningMethod) out = i18n.tr('Non-satellite');
        else if (method === PositionSource.AllPositioningMethods) out = i18n.tr('All/Multiple');
        return out;
    }

    function printableSourceError(method) {
        var out = i18n.tr("Unknown source error");
        if (method === PositionSource.AccessError) out = i18n.tr('Access error');
        else if (method === PositionSource.ClosedError) out = i18n.tr('Closed error');
        else if (method === PositionSource.NoError) out = i18n.tr('No error');
        else if (method === PositionSource.UnknownSourceError) out = i18n.tr('Unidentified source error');
        else if (method === PositionSource.SocketError) out = i18n.tr('Socket error');
        return out;
    }

    function printableOrientation(orientation) {
        var out = i18n.tr("Unknown orientation");
        if (orientation === OrientationReading.TopUp) out = i18n.tr('Top edge is up');
        else if (orientation === OrientationReading.TopDown) out = i18n.tr('Top edge is down');
        else if (orientation === OrientationReading.LeftUp) out = i18n.tr('Left edge is up');
        else if (orientation === OrientationReading.RightUp) out = i18n.tr('Right edge is up');
        else if (orientation === OrientationReading.FaceUp) out = i18n.tr('Face is up');
        else if (orientation === OrientationReading.FaceDown) out = i18n.tr('Face is down');
        return out;
    }

    function round(number, digits) {
        return Math.round(number*Math.pow(10, digits))/Math.pow(10, digits);
    }

    function printAvailableDataRates(availableDataRates){
        var out = i18n.tr("");
        for (var i = 0; i < availableDataRates.length; i++){
            out = qsTr(availableDataRates[i].maximum + " " + out);
        };
        return out;
    }

    MainView {
        id: root
        objectName: 'mainView'
        applicationName: 'sensorsstatus.chrisclime'
        automaticOrientation: true
        anchorToKeyboard: true
        anchors.fill: parent

        PageHeader {
            id: mainPageHeader
            title: parent.title
            trailingActionBar {
                actions: [
                Action {
                    iconName: "info"
                    visible: mainStack.depth === 1
                    text: i18n.tr('About')
                    onTriggered: mainStack.push(aboutPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Accelerometer')
                    onTriggered: mainStack.push(accelerometerPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Magnetometer')
                    onTriggered: mainStack.push(magnetometerPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Altimeter')
                    onTriggered: mainStack.push(altimeterPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Compass')
                    onTriggered: mainStack.push(compassPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('GPS')
                    onTriggered: mainStack.push(gpsPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Gyroscope')
                    onTriggered: mainStack.push(gyroscopePage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Pressure')
                    onTriggered: mainStack.push(pressurePage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Rotation')
                    onTriggered: mainStack.push(rotationPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Orientation')
                    onTriggered: mainStack.push(orientationPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Ambient Light')
                    onTriggered: mainStack.push(ambientLightPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Light')
                    onTriggered: mainStack.push(lightPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Proximity')
                    onTriggered: mainStack.push(proximityPage)
                }, Action {
                    iconName: "torch-on"
                    text: i18n.tr('Temperature')
                    onTriggered: mainStack.push(temperaturePage)
                }
                ]
                numberOfSlots: mainStack.depth === 1 ? 2 : 0
            }
        }

        PageStack {
            id: mainStack
            Component.onCompleted: {
                //mainStack.clear()
                mainStack.push(aboutPage)
            }

            Page {
                id: accelerometerPage
                visible: false
                PageHeader {
                    id: accelerometerPageHeader
                    title: i18n.tr('Accelerometer')
                }
                header: accelerometerPageHeader

                NoData {
                    visible: !accelerometer.connectedToBackend
                }

                Accelerometer {
                    id: accelerometer
                    dataRate: 100
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: accelerometer.connectedToBackend
                    RowField {
                        title: i18n.tr('x (m/s^2)')
                        text: accelerometer.reading !== null ? round(accelerometer.reading.x,1) : '-'
                    }
                    RowField {
                        title: i18n.tr('y (m/s^2)')
                        text: accelerometer.reading !== null ? round(accelerometer.reading.y,1) : '-'
                    }
                    RowField {
                        title: i18n.tr('z (m/s^2)')
                        text: accelerometer.reading !== null ? round(accelerometer.reading.z,1) : '-'
                    }
                    RowField {
                        title: i18n.tr('Active')
                        text: accelerometer.active === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Always on')
                        text: accelerometer.alwaysOn === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Buffer size')
                        text: accelerometer.bufferSize !== null ? accelerometer.bufferSize : '-'
                    }
                    RowField {
                        title: i18n.tr('Efficient buffer size')
                    text: accelerometer.efficientBufferSize !== null ? accelerometer.efficientBufferSize : '-'
                    }
                    RowField {
                        title: i18n.tr('Connected to backend')
                        text: accelerometer.connectedToBackend === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Current orientation')
                        text: accelerometer.currentOrientation !== null ? accelerometer.currentOrientation : '-'
                    }
                    RowField {
                        title: i18n.tr('Data rate (Hz)')
                        text: accelerometer.dataRate !== null ? accelerometer.dataRate : '-'
                    }
                    RowField {
                        title: i18n.tr('Number of available data rates')
                        text: accelerometer.availableDataRates !== null ? accelerometer.availableDataRates.length : '-'
                    }
                    RowField {
                        title: i18n.tr('Available data rates (Hz)')
                        text: accelerometer.availableDataRates !== null ? printAvailableDataRates(accelerometer.availableDataRates) : '-'
                    }
                    RowField {
                        title: i18n.tr('Description')
                        text: accelerometer.description !== null ? accelerometer.description : '-'
                    }
                    RowField {
                        title: i18n.tr('Error code')
                        text: accelerometer.error !== null ? accelerometer.error : '-'
                    }
                    RowField {
                        title: i18n.tr('Sensor identifier')
                        text: accelerometer.identifier !== null ? accelerometer.identifier : '-'
                    }
                    RowField {
                        title: i18n.tr('Output range')
                        text: accelerometer.outputRange !== null ? accelerometer.outputRange : '-'
                    }
                }
            }

            Page {
                id: magnetometerPage
                visible: false
                PageHeader {
                    id: magnetometerPageHeader
                    title: i18n.tr('Magnetometer')
                }
                header: magnetometerPageHeader

                NoData {
                    visible: !magnetometer.connectedToBackend
                }

                Magnetometer {
                    id: magnetometer
                    dataRate: 80
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: magnetometer.connectedToBackend

                    RowField {
                        title: i18n.tr('x')
                        text: magnetometer.reading !== null ? magnetometer.reading.x + i18n.tr(' (') + round(magnetometer.reading.x*1e6,2) + i18n.tr(' micro T)') : '-'
                    }
                    RowField {
                        title: i18n.tr('y')
                        text: magnetometer.reading !== null ? magnetometer.reading.y + i18n.tr(' (') + round(magnetometer.reading.y*1e6,2) + i18n.tr(' micro T)') : '-'
                    }
                    RowField {
                        title: i18n.tr('z')
                        text: magnetometer.reading !== null ? magnetometer.reading.z + i18n.tr(' (') + round(magnetometer.reading.z*1e6,2) + i18n.tr(' micro T)') : '-'
                    }
                    RowField {
                        title: i18n.tr('Calibration level')
                        text: magnetometer.reading !== null && magnetometer.reading.calibrationLevel !== null ? magnetometer.reading.calibrationLevel : '—'
                    }
                    RowField {
                        title: i18n.tr('Active')
                        text: magnetometer.active === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Always on')
                        text: magnetometer.alwaysOn === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Buffer size')
                        text: magnetometer.BufferSize !== null ? magnetometer.bufferSize : '-'
                    }
                    RowField {
                        title: i18n.tr('Efficient buffer size')
                        text: magnetometer.efficientBufferSize !== null ? magnetometer.efficientBufferSize : '-'
                    }
                    RowField {
                        title: i18n.tr('Connected to backend')
                        text: magnetometer.connectedToBackend === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Current orientation')
                        text: magnetometer.currentOrientation !== null ? magnetometer.currentOrientation : '-'
                    }
                    RowField {
                        title: i18n.tr('Number of available data rates')
                        text: accelerometer.availableDataRates !== null ? accelerometer.availableDataRates.length : '-'
                    }
                    RowField {
                        title: i18n.tr('Data rate (Hz)')
                        text: magnetometer.dataRate !== null ? magnetometer.dataRate : '-'
                    }
                    RowField {
                        title: i18n.tr('Available data rates (Hz)')
                        text: magnetometer.availableDataRates !== null ? printAvailableDataRates(magnetometer.availableDataRates) : '-'
                    }
                    RowField {
                        title: i18n.tr('Description')
                        text: magnetometer.description !== null ? magnetometer.description : '-'
                    }
                    RowField {
                        title: i18n.tr('Error code')
                        text: magnetometer.error !== null ? magnetometer.error : '-'
                    }
                    RowField {
                        title: i18n.tr('Sensor identifier')
                        text: magnetometer.identifier !== null ? magnetometer.identifier : '-'
                    }
                    RowField {
                        title: i18n.tr('Output range')
                        text: magnetometer.outputRange !== null ? magnetometer.outputRange : '-'
                    }
                }
            }

            Page {
                id: altimeterPage
                visible: false
                PageHeader {
                    id: altimeterPageHeader
                    title: i18n.tr('Altimeter')
                }
                header: altimeterPageHeader

                NoData {
                    visible: !altimeter.connectedToBackend
                }

                Altimeter {
                    id: altimeter
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: altimeter.connectedToBackend

                    RowField {
                        title: i18n.tr('altitude (m)')
                        text: altimeter.reading !== null ? altimeter.reading.altitude : '—'
                    }
                }
            }

            Page {
                id: compassPage
                visible: false
                PageHeader {
                    id: compassPageHeader
                    title: i18n.tr('Compass')
                }
                header: compassPageHeader

                NoData {
                    visible: !compass.connectedToBackend
                }

                Compass {
                    id: compass
                    active: true
                    }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: compass.connectedToBackend

                    RowField {
                        title: i18n.tr('azimuth (º)')
                        text: compass.reading !== null ? compass.reading.azimuth : '—'
                    }
                    RowField {
                        title: i18n.tr('Calibration level')
                        text: compass.reading !== null ? compass.reading.calibrationLevel : '—'
                    }
                    RowField {
                        title: i18n.tr('Active')
                        text: compass.active === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Always on')
                        text: compass.alwaysOn === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Buffer size')
                        text: compass.bufferSize !== null ? compass.bufferSize : '-'
                    }
                    RowField {
                        title: i18n.tr('Efficient buffer size')
                        text: compass.efficientBufferSize !== null ? compass.efficientBufferSize : '-'
                    }
                    RowField {
                        title: i18n.tr('Connected to backend')
                        text: compass.connectedToBackend === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Current orientation')
                        text: compass.currentOrientation !== null ? compass.currentOrientation : '-'
                    }
                    RowField {
                        title: i18n.tr('Data rate (Hz)')
                        text: compass.dataRate !== null ? compass.dataRate : '-'
                    }
                    RowField {
                        title: i18n.tr('Number of available data rates')
                        text: compass.availableDataRates !== null ? compass.availableDataRates.length : '-'
                    }
                    RowField {
                        title: i18n.tr('Available data rates (Hz)')
                        text: compass.availableDataRates !== null ? printAvailableDataRates(compass.availableDataRates) : '-'
                    }
                    RowField {
                        title: i18n.tr('Description')
                        text: compass.description !== null ? compass.description : '-'
                    }
                    RowField {
                        title: i18n.tr('Error code')
                        text: compass.error !== null ? compass.error : '-'
                    }
                    RowField {
                        title: i18n.tr('Sensor identifier')
                        text: compass.identifier !== null ? compass.identifier : '-'
                    }
                    RowField {
                        title: i18n.tr('Output range')
                        text: compass.outputRange !== null ? compass.outputRange : '-'
                    }
                }
            }

            Page {
                id: gpsPage
                visible: false
                PageHeader {
                    id: gpsPageHeader
                    title: i18n.tr('GPS')
                }
                header: gpsPageHeader

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }

                    Row {
                        width: parent.width
                        spacing: units.gu(1)
                        anchors.margins: units.gu(0.5)

                        Label {
                            text: i18n.tr('Found supported backend')
                            anchors.verticalCenter: backendFound.verticalCenter
                        }

                        CheckBox {
                            id: backendFound
                            checked: geoposition.valid
                            enabled: false
                        }
                    }
                    Row {
                        width: parent.width
                        spacing: units.gu(1)
                        anchors.margins: units.gu(0.5)

                        Label {
                            text: i18n.tr('Active')
                            anchors.verticalCenter: backendActive.verticalCenter
                        }

                        CheckBox {
                            id: backendActive
                            checked: geoposition.active
                            enabled: false
                        }
                    }

                    RowField {
                        title: i18n.tr('Source status')
                        text: printableSourceError(geoposition.sourceError)
                    }
                    RowField {
                        title: i18n.tr('Name')
                        text: geoposition.name
                    }
                    RowField {
                        title: i18n.tr('Method')
                        text: printablePositionMethod(geoposition.positioningMethod)
                    }
                    RowField {
                        title: i18n.tr('Latitude (º)')
                        text: geoposition.position.coordinate.latitude || '—'
                    }
                    RowField {
                        title: i18n.tr('Longitude (º)')
                        text: geoposition.position.coordinate.longitude || '—'
                    }
                    RowField {
                        title: i18n.tr('Horizontal accuracy (m)')
                        text: geoposition.position.horizontalAccuracy || '—'
                    }
                    RowField {
                        title: i18n.tr('Altitude (m)')
                        text: geoposition.position.coordinate.altitude || '—'
                    }
                    RowField {
                        title: i18n.tr('Vertical accuracy (m)')
                        text: geoposition.position.verticalAccuracy || '—'
                    }
                    RowField {
                        title: i18n.tr('Speed (m/s)')
                        text: geoposition.position.speed === -1 ? '—' : geoposition.position.speed
                    }
                    RowField {
                        title: i18n.tr('Last updated')
                        text: geoposition.position.timestamp ? geoposition.position.timestamp : '—'
                    }
                    PositionSource {
                        id: geoposition
                        active: true
                        preferredPositioningMethods: PositionSource.SatellitePositioningMethods
                    }
                }
            }

            Page {
                id: gyroscopePage
                visible: false
                PageHeader {
                    id: gyroscopePageHeader
                    title: i18n.tr('Gyroscope')
                }
                header: gyroscopePageHeader

                NoData {
                    visible: !gyroscope.connectedToBackend
                }

                Gyroscope {
                    id: gyroscope
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: gyroscope.connectedToBackend

                    RowField {
                        title: i18n.tr('x (º/s)')
                        text: gyroscope.reading !== null ? gyroscope.reading.x : '-'
                    }
                    RowField {
                        title: i18n.tr('y (º/s)')
                        text: gyroscope.reading !== null ? gyroscope.reading.y : '-'
                    }
                    RowField {
                        title: i18n.tr('z (º/s)')
                        text: gyroscope.reading !== null ? gyroscope.reading.z : '-'
                    }
                    RowField {
                        title: i18n.tr('Active')
                        text: gyroscope.active === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Always on')
                        text: gyroscope.alwaysOn === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Buffer size')
                        text: gyroscope.bufferSize !== null ? gyroscope.bufferSize : '-'
                    }
                    RowField {
                        title: i18n.tr('Efficient buffer size')
                    text: gyroscope.efficientBufferSize !== null ? gyroscope.efficientBufferSize : '-'
                    }
                    RowField {
                        title: i18n.tr('Connected to backend')
                        text: gyroscope.connectedToBackend === true ? i18n.tr('true'): i18n.tr('false')
                    }
                    RowField {
                        title: i18n.tr('Current orientation')
                        text: gyroscope.currentOrientation !== null ? gyroscope.currentOrientation : '-'
                    }
                    RowField {
                        title: i18n.tr('Data rate (Hz)')
                        text: gyroscope.dataRate !== null ? gyroscope.dataRate : '-'
                    }
                    RowField {
                        title: i18n.tr('Number of available data rates')
                        text: gyroscope.availableDataRates !== null ? gyroscope.availableDataRates.length : '-'
                    }
                    RowField {
                        title: i18n.tr('Available data rates (Hz)')
                        text: gyroscope.availableDataRates !== null ? printAvailableDataRates(gyroscope.availableDataRates) : '-'
                    }
                    RowField {
                        title: i18n.tr('Description')
                        text: gyroscope.description !== null ? gyroscope.description : '-'
                    }
                    RowField {
                        title: i18n.tr('Error code')
                        text: gyroscope.error !== null ? gyroscope.error : '-'
                    }
                    RowField {
                        title: i18n.tr('Sensor identifier')
                        text: gyroscope.identifier !== null ? gyroscope.identifier : '-'
                    }
                    RowField {
                        title: i18n.tr('Output range')
                        text: gyroscope.outputRange !== null ? gyroscope.outputRange : '-'
                    }
                }
            }

            Page {
                id: pressurePage
                visible: false
                PageHeader {
                    id: pressurePageHeader
                    title: i18n.tr('Pressure')
                }
                header: pressurePageHeader

                NoData {
                    visible: !pressure.connectedToBackend
                }

                PressureSensor {
                    id: pressure
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: pressure.connectedToBackend

                    RowField {
                        title: i18n.tr('pressure (Pa)')
                        text: pressure.reading !== null ? pressure.reading.pressure : '-'
                    }
                }
            }

            Page {
                id: rotationPage
                visible: false
                PageHeader {
                    id: rotationPageHeader
                    title: i18n.tr('Rotation')
                }
                header: rotationPageHeader

                NoData {
                    visible: !rotation.connectedToBackend
                }

                RotationSensor {
                    id: rotation
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                        }
                    visible: rotation.connectedToBackend

                    RowField {
                        title: i18n.tr('x [°]')
                        text: rotation.reading !== null ? round(rotation.reading.x, 1) : '-'
                    }
                    RowField {
                        title: i18n.tr('y [°]')
                        text: rotation.reading !== null ? round(rotation.reading.y, 1) : '-'
                    }
                    RowField {
                        title: i18n.tr('z [°]')
                        visible: rotation.hasZ
                        text: rotation.reading !== null ? round(rotation.reading.z, 1) : '-'
                    }
                }
            }

            Page {
                id: orientationPage
                visible: false
                PageHeader {
                    id: orientationPageHeader
                    title: i18n.tr('Orientation')
                }
                header: orientationPageHeader

                NoData {
                    visible: !orientation.connectedToBackend
                }

                OrientationSensor {
                    id: orientation
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: orientation.connectedToBackend

                    RowField {
                        title: i18n.tr('Orientation')
                        text: orientation.reading !== null ? printableOrientation(orientation.reading.orientation) : '-'
                    }
                }
            }

            Page {
                id: ambientLightPage
                visible: false
                PageHeader {
                    id: ambientLightPageHeader
                    title: i18n.tr('Ambient Light')
                }
                header: ambientLightPageHeader

                NoData {
                    visible: !ambientLight.connectedToBackend
                }

                AmbientLightSensor {
                    id: ambientLight
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: ambientLight.connectedToBackend

                    RowField {
                        title: i18n.tr('Ambient Light')
                        text: ambientLight.reading !== null ? ambientLight.reading.lightLevel : '-'
                    }
                }
            }

            Page {
                id: lightPage
                visible: false
                PageHeader {
                    id: lightPageHeader
                    title: i18n.tr('Light')
                }
                header: lightPageHeader

                NoData {
                    visible: !light.connectedToBackend
                }

                LightSensor {
                    id: light
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: light.connectedToBackend

                    RowField {
                        title: i18n.tr('Light')
                        text: light.reading !== null ? light.reading.illuminance : '-'
                    }
                }
            }

            Page {
                id: proximityPage
                visible: false
                PageHeader {
                    id: proximityPageHeader
                    title: i18n.tr('Proximity')
                }
                header: proximityPageHeader

                NoData {
                    visible: !proximity.connectedToBackend
                }

                ProximitySensor {
                    id: proximity
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                    anchors {
                        margins: units.gu(2)
                        topMargin: units.gu(7)
                        fill: parent
                    }
                    visible: proximity.connectedToBackend

                    RowField {
                        title: i18n.tr('Proximity near:')
                        text: proximity.reading !== null ? proximity.reading.near : '-'
                    }
                }
            }

            Page {
                id: temperaturePage
                visible: false
                PageHeader {
                    id: temperaturePageHeader
                    title: i18n.tr('Temperature')
                }
                header: temperaturePageHeader

                NoData {
                    visible: !temperature.connectedToBackend
                }

                AmbientTemperatureSensor {
                    id: temperature
                    active: true
                }

                Column {
                    spacing: units.gu(1)
                        anchors {
                            margins: units.gu(2)
                            topMargin: units.gu(7)
                            fill: parent
                        }
                    visible: temperature.connectedToBackend

                    RowField {
                        title: i18n.tr('Temperature')
                        text: temperature.reading !== null ? temperature.reading.temperature : '-'
                    }
                }
            }

            Page {
                id: aboutPage
                visible: false
                title: i18n.tr('About SensorsStatus')
                header: mainPageHeader
//                PageHeader {
//                    id: aboutPageHeader
//                    title: i18n.tr('About SensorsStatus')
//                }
//                header: aboutPageHeader

                Column {
                    spacing: units.gu(1)
                        anchors {
                            margins: units.gu(2)
                            topMargin: units.gu(7)
                            fill: parent
                        }

                    RowField {
                        text: 'SensorsStatus app'
                    }
                    RowField {
                        text: 'Designed to check the status of the sensors'
                    }
                }
            }
        }
    }
}